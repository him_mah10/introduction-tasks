#include <iostream>
#include <cstdio>
#include "cassandra.h"
#include <fstream>
#include <string.h>
#include <vector> 
#include <uv.h>
using namespace std;

class FpgaRowCache
{
	private:
		string keyspace;
		string tablename;
		string host;
	  	int port;
	  	CassCluster* cluster = NULL;
	  	CassSession* session = NULL;
	  	void readCfg();
	  	CassError execute_query(CassSession* session, string query);
	  	void init();
	  	void print_error(CassFuture* future);

	 public:
	 	FpgaRowCache();
	 	void createSession();
	 	void close();
	 	void clear();
	 	CassError put(const char* key, const char* value);
	 	const char* get(const char* key);
	 	bool containsKey(const char* key);
	 	bool putIfAbsent(const char * key, const char * value);
	 	bool replace(const char* key, const char* old, const char* value);
	 	void remove(const char* key);
};

CassError FpgaRowCache::execute_query(CassSession* session, string query) 
{
  CassError rc = CASS_OK;
  CassStatement* statement = cass_statement_new((const char*)query.c_str(), 0);
  CassFuture* future = cass_session_execute(session, statement);

  cass_future_wait(future);

  rc = cass_future_error_code(future);
  if (rc != CASS_OK) {
    cerr<<"There was an error in executing the query\n";
  }

  cass_future_free(future);
  cass_statement_free(statement);

  return rc;
}

void FpgaRowCache::readCfg() 
{
    string rc_cfg = "/home/reniac/rowcache.cfg";
    try 
    {
		ifstream ifs(rc_cfg.c_str());
		if(ifs.is_open())
		{
			getline(ifs,host);
			string temp_port="";
			getline(ifs,temp_port);
			if(host=="" || temp_port=="")
				throw 404;
			port=0;
			for(int i=0;i<temp_port.size();i++)
			{
				port=port*10+int(temp_port[i]-'0');
			}
			// port=stoi(temp_port);
		}
		else 
	    {
	      throw 404;
	    }
	}
  	catch (int myNum) 
  	{
	    cerr<<"Unable to read " + rc_cfg + " properly.\n";  
  	}
}


void FpgaRowCache::createSession() 
{
    readCfg();
    cout<<host<<" "<<port<<endl;
    session=cass_session_new();
    cluster = cass_cluster_new();
  	
  	cass_cluster_set_contact_points(cluster, (const char*)host.c_str());
  	cass_cluster_set_port(cluster,port);

  	cass_cluster_set_core_connections_per_host(cluster,16);
  	cass_cluster_set_max_requests_per_flush(cluster,4096);
  	cass_cluster_set_max_concurrent_requests_threshold(cluster,4096);
  	cass_cluster_set_max_connections_per_host(cluster,16);
  	
  	cout<<"Connecting to : "<<host<<":"<<port<<endl;
  	CassFuture* future = cass_session_connect(session, cluster);
  	cass_future_wait(future);
  	CassError rc = CASS_OK;
  	rc = cass_future_error_code(future);
  	if (rc != CASS_OK) 
  	{
    	cerr<<"Error in connection. The error is \n";
  	}
  	cass_future_free(future);
}

void FpgaRowCache::close() 
{
    cass_session_close(session);
    // cass_cluster_close(cluster);
    cass_cluster_free(cluster);
    // cluster.close();
}

FpgaRowCache::FpgaRowCache() 
{
	// cout<<"Yes, here\n";
	keyspace = "rowcache";
	tablename= "kvstore";
	host="127.0.0.1";
  port = 9042;
	createSession();
	init();
}

void FpgaRowCache::clear() 
{
	init();
}

void FpgaRowCache::init() 
{
	
	execute_query(session,"DROP KEYSPACE IF EXISTS " + keyspace + ";");
	execute_query(session,"CREATE KEYSPACE IF NOT EXISTS " + keyspace + " WITH replication = { 'class': 'SimpleStrategy', 'replication_factor': '1' };");
	cout<<"Created Keyspace : " + keyspace<<endl;
	// execute_query(session,"DROP TABLE IF EXISTS " + tablename + ";");
	execute_query(session,"CREATE TABLE IF NOT EXISTS " + keyspace + "." + tablename + " (id VARCHAR PRIMARY KEY, value VARCHAR);" );
	cout<<"Created Table : " + tablename<<endl;
}

CassError FpgaRowCache::put(const char* key,const char * value) 
{
	CassError rc = CASS_OK;
	CassStatement* statement = NULL;
	CassFuture* future =NULL;

	string temp="INSERT INTO "+ keyspace+"."+tablename+" (id,value) VALUES(?,?);";
	const char* query = (char*)malloc(sizeof(char)*temp.size());
	query = (const char*)temp.c_str();
	
	statement = cass_statement_new(query, 2);
	cass_statement_bind_string(statement, 0, key);
	cass_statement_bind_string(statement, 1, value);
	future =  cass_session_execute(session, statement);

	cass_future_wait(future);
	rc = cass_future_error_code(future);
	if (rc != CASS_OK) 
	{
   		print_error(future);
	}
  	cass_future_free(future);
  	// delete query;
  	cass_statement_free(statement);
  	return rc;
}

void FpgaRowCache::print_error(CassFuture* future) 
{
  const char* message;
  size_t message_length;
  // cout<<future.error().code<<endl;
  cass_future_error_message(future, &message, &message_length);
  fprintf(stderr, "Error: %.*s\n", (int)message_length, message);
  // delete message;
}

bool FpgaRowCache::putIfAbsent(const char * key, const char * value) 
{
	if(containsKey(key))
    	return false;
    put(key, value);
    return true;
}

const char* FpgaRowCache::get(const char * key) 
{
	CassError rc = CASS_OK;
	CassStatement* statement = NULL;
  CassFuture* future = NULL;

  string temp = "SELECT value FROM "+ keyspace+"."+tablename+" WHERE id = ?";
	const char* query = (char*)malloc(sizeof(char)*temp.size());
	query = (const char*)temp.c_str();

  statement = cass_statement_new(query, 1);

  cass_statement_bind_string_by_name(statement, "id", key);

  future = cass_session_execute(session, statement);
  cass_future_wait(future);
    
  const char* st;
  size_t len;
    
  rc = cass_future_error_code(future);
  if (rc != CASS_OK) 
  {
    print_error(future);
  } 
  else 
  {
  	const CassResult* result = cass_future_get_result(future);
  	CassIterator* iterator = cass_iterator_from_result(result);

  	if (cass_iterator_next(iterator)) 
   	{
     	const CassRow* row = cass_iterator_get_row(iterator);
	    cass_value_get_string(cass_row_get_column_by_name(row, "value"), &st,&len);
	 	}

	  cass_result_free(result);
	  cass_iterator_free(iterator);
  }

  cass_future_free(future);
  cass_statement_free(statement);
  	

  char* tt=(char*)malloc(sizeof (char) * (len+1));
  memcpy(tt, st, len);
  tt[len] = '\0';
  cout<<"Inside Get: "<<st<<"\nLength: "<<len<<endl;
  return (const char*)tt;	
}

bool FpgaRowCache::containsKey(const char* key) 
{
	const char* v = get(key);
  return v != NULL;
}

bool FpgaRowCache::replace(const char* key,const char* old,const char* value) 
{
    const char* v = get(key);
    cout<<"\nInside Replace: "<<v<<endl;
    if (v == NULL || strcmp(old,v)==0) 
    {
    	cout<<"Key: "<<key<<"\nOld: "<<old<<"\nValue: "<<value<<"\nDB Value: "<<v<<endl;
    	remove(key);
    	put(key, value);
	    return true;
    }
    else
    {
    }
    return false;
}

void FpgaRowCache::remove(const char* key) 
{
	CassStatement* statement = NULL;
	string temp = "DELETE FROM "+ keyspace+"."+tablename+"  WHERE id = ?";
	const char* query = (char*)malloc(sizeof(char)*temp.size());
	query = (const char*)temp.c_str();
	statement = cass_statement_new(query, 1);
    cass_statement_bind_string(statement, 0, key);
    // cout<<"---- "<<strlen(key)<<endl;
	CassFuture* future = cass_session_execute(session, statement);
	
	CassError rc = CASS_OK;
	cass_future_wait(future);
	rc = cass_future_error_code(future);
	if (rc != CASS_OK) 
	{
   		print_error(future);
	}
  	cass_future_free(future);
  	cass_statement_free(statement);
}