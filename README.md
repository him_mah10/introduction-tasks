# Introduction Tasks #

### Task 1 ###

1. Download cassandra 3.11.4 version binary
2. Start cassandra
3. Download C++ cassandra drivers (https://docs.datastax.com/en/developer/cpp-driver-dse/1.10/)
4. Run this example: https://github.com/datastax/cpp-dse-driver-examples/blob/master/core/examples/prepared/prepared.c

**Update -** Completed

### Task 2 ###
1. Implement FpgaRowCache.java in C++
2. Test it enough by doing 10-100 Million transactions by writing tests in c++

**Update -** Both FpgaRowCache.cpp and test.cpp files are uploaded and testing is done. Sometimes the replace doesn't work immediately maybe because of the ttl invovled. Thus the test for it fails about 12.7% times.