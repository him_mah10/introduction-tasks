#include <stdlib.h>
#include "FpgaRowCache.cpp"
#include <map> 

using namespace std;

string random_string(int stringLength)
{
	string key = "";
	string letters = "";
	for(char i = 'a';i<='z';i++)letters+=i;
	for(char i = 'A';i<='Z';i++)letters+=i;
	for(int i=0;i<stringLength;i++)
		key = key + letters[rand()%52];
	return key;
}

FpgaRowCache fpga;
map< string, string> db;
int db_size = 0;
int num = 0;

int main(int argc,char* argv[])
{
	int cc=0;
	cout<<"Inserting 100000 elements\n";
	for(int i=0;i<1000;i++)
	{
		int k_len = rand()%64 + 1;
		int v_len = rand()%256 + 1;
		
		string k_temp = random_string(k_len);
		const char* key = (char*)malloc(sizeof(char)*k_temp.size());
		key = (const char*)k_temp.c_str();

		string v_temp = random_string(v_len);
		const char* value = (char*)malloc(sizeof(char)*v_temp.size());
		value = (const char*)v_temp.c_str();
		
		db.insert(pair< string, string>(k_temp,v_temp));
		fpga.put(key,value);
		
		db_size++;
	}
	// cout<<"----- "<<cc<<" -----";

	cout<<"Insertion done\n\nRunning 1000000 tests\n";
	int fail_count=0;
	for(int i=0;i<100;i++)
	{
		// int a;
		// cin>>a;
		int x = rand()%4;
		x=3;
		if(x==0)
		{
			/*
				Checks get functionality
				Generates a random key, check if its there in the database
				If yes then output correct, else output false 
			*/

			cout<<"Checking get functionality\n";
			string k = random_string(rand()%64+1);
			const char* ans = fpga.get((const char*)k.c_str());
			map<string,string>::iterator itr = db.find(k);
			if((ans==NULL && itr == db.end()) || (ans!=NULL && itr->second == ans) )
				cout<<"Correct\n";
			else
			{
				fail_count++;
				cout<<"Incorrect\n";
			}
			cout<<endl;
		}
		else if(x==1)
		{
			/*
				Checks put and replace functionality
				Generates a key and value, checks if key is there in the database
				If yes then replace them otherwise insert.
			*/

			int k = rand()%64 + 1;
			int v = rand()%256 + 1;
			string k_temp = random_string(k);
			const char* key = (char*)malloc(sizeof(char)*k_temp.size());
			key = (const char*)k_temp.c_str();
			// cout<<key<<endl;
			string v_temp = random_string(v);
			const char* value = (char*)malloc(sizeof(char)*v_temp.size()); 
			value = (const char*)v_temp.c_str();

			
			map<string,string>::iterator itr = db.find(key);

			const char* ans = fpga.get(key);
			if(ans==NULL)
			{
				cout<<"New key, inserting.\n";
				fpga.put(key,value);
			}
			else
			{
				cout<<"Key already exist, overwriting\n";
				fpga.replace(key,(const char*)ans,value);
			}
			db.insert(pair<string ,string>(key,value));
			cout<<endl;
			db_size++;	
		}
		else if(x==2)
		{
			/*
				Checks remove functionality
				Selects a random string to be removed and then remove it.
			*/

			int max_size = db.size();
			int rem = rand()%max_size;
			map<string,string>::iterator itr = db.begin();
			for(int i=0;i<rem;i++)itr++;

			const char* key = (char*)malloc(sizeof(char)*itr->first.size());
			key = (const char*)itr->first.c_str();
			const char* aa = fpga.get(key);

			fpga.remove(key);

			db_size--;
			db.erase(itr);
			
			// const char* ans = fpga.get(key);
			// if(ans==NULL)
			// 	cout<<"Remove Successful!\n";
			// else
			// {
			// 	fail_count++;
			// 	cout<<"Remove Failed :( \n";
			// }
			cout<<endl;
		}
		else if(x==3)
		{
			/*
				Checks Replace functionality
				Selects a random key, and takes it new value from the user and update it.
			*/

			int max_size = db.size();
			int rem = rand()%max_size;
			map<string,string>::iterator itr = db.begin();
			for(int i=0;i<rem;i++)itr++;
			
			const char* key = (char*)malloc(sizeof(char)*itr->first.size());
			key = (const char*)itr->first.c_str();
			const char* value = (char*)malloc(sizeof(char)*itr->second.size()); 
			value = (const char*)itr->second.c_str();
			string new_value;

			string v_temp = random_string(rand()%256 + 1);
			const char* new_val = (char*)malloc(sizeof(char)*v_temp.size()); 
			new_val = (const char*)v_temp.c_str();


			// bool ret = fpga.replace(key,value,new_val);
			bool ret=true;	
			const char* ans=fpga.get(key);
			
			if(strcmp(ans,value)==0 && ret)
			{
				// cout<<"Replace Successful\n";
				// itr->second=new_val;
			}
			else
			{
				cout<<"--------------------------------\n";
				cout<<"Key: "<<key;
				cout<<"\nGet Value:  "<<ans<<endl;
				cout<<"Real Value: "<<value<<endl;
				cout<<"Real Size: "<<strlen(value)<<endl;
				fail_count++;
				cout<<"Replace Failed\n";
				cout<<"--------------------------------\n";
			}
			cout<<endl;
		}
	}
	cout<<"Number of failed events: "<<fail_count<<endl;
	return 0;
}