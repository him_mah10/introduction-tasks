package org.apache.cassandra.cache;

import java.util.*;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.io.*;

import com.datastax.driver.core.Session;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Statement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.*;
import com.datastax.driver.core.policies.*;
import com.datastax.driver.core.querybuilder.*;

//import com.datastax.oss.driver.api.core.cql.SimpleStatement;
//import com.datastax.oss.driver.api.querybuilder.QueryBuilder;
//import com.datastax.driver.core.querybuilder.QueryBuilder;
//import com.datastax.oss.driver.api.querybuilder.QueryBuilder.*;
//import com.datastax.oss.driver.api.querybuilder.select.Select;
//import com.datastax.driver.core.schemabuilder.SchemaBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


class FpgaRowCache {
	String keyspace = "rowcache";
	String tablename= "kvstore";
  
  //new13 172.16.0.23
  //String host="172.16.0.23";
  //int port = 9042;
  
  //egypt on new13
  //String host="172.16.0.205";
  //int port = 8002;
  
  //new20
  String host="172.16.0.25";
  int port = 9042;
  
  //dell08 172.16.0.40
  //String host="172.16.0.40";
  //int port = 9042;
  
  //String host="127.0.0.1";
  //int port = 9042;
  
  Cluster cluster = null;
  Session session = null;
        
  private static final Logger logger = LoggerFactory.getLogger(FpgaRowCache.class);

  private void readCfg() {
    String rc_cfg = "/home/reniac/rowcache.cfg";
    BufferedReader reader;
    try {
      reader = new BufferedReader(new FileReader(rc_cfg));
      host  = reader.readLine();
      port  = Integer.parseInt(reader.readLine());
      reader.close();
    } catch (IOException e) {
      logger.error("Unable to read " + rc_cfg + " properly");
    }
  }
 
  private void print_config(Cluster.Builder b) {
    Configuration cfg = session.getCluster().getConfiguration();
    PoolingOptions pOpt = cfg.getPoolingOptions();
    logger.info("pOpt.getCoreConnectionsPerHost(HostDistance.LOCAL) : " + pOpt.getCoreConnectionsPerHost(HostDistance.LOCAL));
    logger.info("pOpt.getMaxConnectionsPerHost(HostDistance.LOCAL) : " + pOpt.getMaxConnectionsPerHost(HostDistance.LOCAL));
    logger.info("pOpt.getMaxRequestsPerConnection(HostDistance.LOCAL) : " + pOpt.getMaxRequestsPerConnection(HostDistance.LOCAL));
    logger.info("pOpt.getCoreConnectionsPerHost(HostDistance.REMOTE) : " + pOpt.getCoreConnectionsPerHost(HostDistance.REMOTE));
    logger.info("pOpt.getMaxConnectionsPerHost(HostDistance.REMOTE) : " + pOpt.getMaxConnectionsPerHost(HostDistance.REMOTE));
    logger.info("pOpt.getMaxRequestsPerConnection(HostDistance.REMOTE) : " + pOpt.getMaxRequestsPerConnection(HostDistance.REMOTE));
    //logger.info("pOpt : " + pOpt);
    //logger.info("pOpt.getMaxQueueSize() : " + pOpt.getMaxQueueSize());
    LoadBalancingPolicy lp = cfg.getPolicies(). getLoadBalancingPolicy();
    Set<Host> hosts = session.getCluster().getMetadata().getAllHosts();
    Iterator<Host> iter = hosts.iterator();
    while (iter.hasNext()) { 
      Host host = iter.next();
      HostDistance hd = lp.distance(host);
      logger.info("Host = " + host);
      logger.info("HostDistance = " + hd);
    }
  }

  public void createSession() {
    readCfg();
    
    PoolingOptions pOpt = new PoolingOptions();
    pOpt.setMaxRequestsPerConnection(HostDistance.LOCAL, 4*1024);
    pOpt.setMaxRequestsPerConnection(HostDistance.REMOTE, 4*1024);
    pOpt.setCoreConnectionsPerHost(HostDistance.LOCAL, 16);
    pOpt.setCoreConnectionsPerHost(HostDistance.REMOTE, 16);
    pOpt.setMaxConnectionsPerHost(HostDistance.LOCAL, 16);
    pOpt.setMaxConnectionsPerHost(HostDistance.REMOTE, 16);


    Cluster.Builder builder = Cluster.builder()
                                .withPoolingOptions(pOpt);

    cluster = builder
                .addContactPoint(host).withPort(port)
                .build();
    logger.info("Connecting to : " + host + ":" + port);
    session = cluster.connect();
    print_config(builder);
    /*session = CqlSession.builder()
        .addContactPoint(new InetSocketAddress(host, port))
        .withLocalDatacenter("datacenter1")
        .build();*/
  }

  public void close() {
    session.close();
    cluster.close();
  }

	public FpgaRowCache create()
	{
		return new FpgaRowCache();
	}
	
	public FpgaRowCache() {
    createSession();
    init();
  }

  private void init() {
			session.execute("DROP KEYSPACE IF EXISTS " + keyspace + ";");
			
      ResultSet rs = session.execute(
				"CREATE KEYSPACE IF NOT EXISTS " + keyspace + " WITH "
				+ "	replication = { 'class': 'SimpleStrategy', 'replication_factor': '1' };"
			);
			print("Create Keyspace:" + rs.one());
			ResultSet rs2 = session.execute(
				"CREATE TABLE IF NOT EXISTS " + keyspace + "." + tablename
				+ " (id blob PRIMARY KEY, value blob);"
			);
			print("Create Table:" + rs2.one());
	}
	
	public void put(ByteBuffer key, ByteBuffer value) {
	  //logger.info("put k=" + key + " v=" + value);
    Statement q=QueryBuilder.insertInto(keyspace, tablename).value("id", key).value("value", value);
	  ResultSet rs = session.execute(q);
/*				"INSERT INTO " + keyspace +"." + tablename
					+ " (id, value) VALUES ('"
					+ key + "','" + value + "');"
				);*/
	  Row row = rs.one();
	  /*{
      print("put:" + Integer.toString(key.getInt()) + "->" + Integer.toString(value.getInt()));
      key.flip();
      value.flip();
    }*/
	}
	
	public boolean putIfAbsent(ByteBuffer key, ByteBuffer value) {
		if(containsKey(key))
      return false;
    
    put(key, value);
    return true;
	}

  //does a put only if key dosen't exist or has the old value
	public boolean replace(ByteBuffer key, ByteBuffer old, ByteBuffer value) {
    //System.out.println("1.0 key=" + key);
		ByteBuffer v = get(key);
    //System.out.println("2.0 key=" + key);
    if (v == null || v.equals(old)) {
      put(key, value);
      //System.out.println("3.0 key=" + key);
      return true;
    } 
    return false;
	}
	
	public ByteBuffer get(ByteBuffer key) {
	  Statement q=QueryBuilder.select("value").from(keyspace, tablename).where(QueryBuilder.eq("id", key));
    ResultSet rs = session.execute(q);
/*				"SELECT value from " + keyspace +"." + tablename
				+ " WHERE id = '" + key + "'"
			);*/
    Row row = rs.one();
    //System.out.println("get row=" + row);
    if(row == null) return null;
    
    ByteBuffer value = null;
    value = row.getBytes("value");
	  //logger.info("get k=" + key + " v=" + value);
	  /*{ 
    print("get:" + Integer.toString(key.getInt()) + "->" + Integer.toString(value.getInt()));
    value.flip();
    key.flip();
    }*/
    return value;
  }

	    
	public void remove(ByteBuffer key) {
    //System.out.println("1.0 key=" + key);
	  Statement q=QueryBuilder.delete().from(keyspace, tablename).where(QueryBuilder.eq("id", key));
    //System.out.println("2.0 key=" + key);
	  ResultSet rs = session.execute(q);
/*      "DELETE FROM " + keyspace +"." + tablename
				+ " WHERE id = '" + key + "'"
			);*/
    Row row = rs.one();
    //print(row);
  }
	
  public void clear() {
	  init();
  }
	
/*  public Iterator<String> keyIterator() {}
	public Iterator<String> hotKeyIterator(int n){}*/
	public boolean containsKey(ByteBuffer key) {
		ByteBuffer v = get(key);
    return v != null;
  }

  private void print(Object msg) {
    return;
    //System.out.println("CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC: " + msg);
  }

};
